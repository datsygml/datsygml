"""DjangoDatsy URL Configuration
"""

from django.urls import include, path

urlpatterns = [
    path('aws/', include('aws.urls')),
]
