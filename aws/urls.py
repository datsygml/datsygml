from django.urls import include, path

from . import views

urlpatterns = [
    path('api/v1/dasty-instance/start', views.StartInstance.as_view(), name="Starts the EC2 instance"),
    path('api/v1/dasty-instance/stop', views.StopInstance.as_view(), name="Stops the EC2 instance"),
    path('api/v1/dasty-instance/backup', views.CreateBackupImage.as_view(), name="Creates a backup image"),
    path('api/v1/dasty-instance/createInstance', views.CreateInstace.as_view(), name="Creates instance from backup image"),
    path('api/v1/dasty-instance/copy', views.CopyInstace.as_view(), name="Copies an AMI image to a given region"),
]