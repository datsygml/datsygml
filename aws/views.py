# Python imports
import boto3, datetime, pprint

# Django imports


# Django Rest API imports
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

def get_session(region):
    return boto3.session.Session(region_name=region)

session = get_session('us-east-2')
client = session.client('ec2')

demo = client.describe_instances(Filters=[{'Name': 'tag:Name', 'Values': ['demo-instance']}])
# print("demo:")
# pprint.pprint(demo)

# To check the state of our instance
demo_state = demo['Reservations'][0]['Instances'][0]['State']
# print("demo_state:", demo_state)

# Instance ID
instance_id = demo['Reservations'][0]['Instances'][0]['InstanceId']
print("instance_id:", instance_id)

class StartInstance(APIView):
    """
    API end-points for starting EC2 instance
    """
    def post(self, request):
        """
        Method to start an EC2 instance
        """
        instance_id = self.request.data['instance_id']

        try:
            # Starting an instance (Need to check if the instance exists)
            startInstance = client.start_instances(InstanceIds=[instance_id])

            if startInstance["ResponseMetadata"]["HTTPStatusCode"] == 200:       
                return Response({"Message": "SUCCESS"}, status=status.HTTP_200_OK)
            return Response({"Message": "FAIL"}, status=status.HTTP_400_BAD_REQUEST)

        except:
            # Handling the case of incorrect instance id (instance not found)
            return Response({"Message": "FAIL"}, status=status.HTTP_404_NOT_FOUND)     


class StopInstance(APIView):
    """
    API end-points for stopping EC2 instance
    """
    def post(self, request):
        """
        Method to stop an EC2 instance
        """
        instance_id = self.request.data['instance_id']

        try:   
             # Stopping instance  
            stopInstance = client.stop_instances(InstanceIds=[instance_id])

            if stopInstance["ResponseMetadata"]["HTTPStatusCode"] == 200:       
                return Response({"Message": "SUCCESS"}, status=status.HTTP_200_OK)
            return Response({"Message": "FAIL"}, status=status.HTTP_400_BAD_REQUEST)

        except:
            # Handling the case of incorrect instance id (instance not found)
            return Response({"Message": "FAIL"}, status=status.HTTP_404_NOT_FOUND)    


class CreateBackupImage(APIView):
    """
    API end-points for creating backup image of a EC2 instance
    """
    def post(self, request):
        """
        Method to create back-up image of EC2 instance
        """
        instance_id = self.request.data['instance_id']
        print("instance_id to create backup image:", instance_id)

        date = datetime.datetime.now().strftime("%d%m%Y%H%M%S")
        name = "InstanceId_" + instance_id + "_BackupImage_" + str(date)

        try:
            # Creating backup image for given AMI(`instance_id`)    
            createImage = client.create_image(InstanceId=instance_id, Name=name)

            if createImage["ResponseMetadata"]["HTTPStatusCode"] == 200:       
                return Response({"Message": "SUCCESS"}, status=status.HTTP_200_OK)
            return Response({"Message": "FAIL"}, status=status.HTTP_400_BAD_REQUEST)

        except:
            # Handling the case of incorrect instance id (instance not found)
            return Response({"Message": "FAIL"}, status=status.HTTP_404_NOT_FOUND)    


class CreateInstace(APIView):
    """
    API to create instance from a backup image
    """
    def post(self, request):
        """
        Method to create instance from image
        """
        image_id = self.request.data['image_id']
        print("image_id to create new instance:", image_id)

        try:
            # Create instance from given `image_id`    
            createInstace = client.run_instances(ImageId=image_id, MinCount=1, MaxCount=1, InstanceType='t2.micro')

            if createInstace["ResponseMetadata"]["HTTPStatusCode"] == 200:       
                return Response({"apiKey": "", "appId": ""}, status=status.HTTP_200_OK)

            elif createInstace["ResponseMetadata"]["HTTPStatusCode"] == 400:
                return Response({"Message": "BAD-REQUEST"}, status=status.HTTP_400_BAD_REQUEST)

            elif createInstace["ResponseMetadata"]["HTTPStatusCode"] == 401:
                return Response({"Message": "UNAUTHORIZED"}, status=status.HTTP_401_UNAUTHORIZED)

            elif createInstace["ResponseMetadata"]["HTTPStatusCode"] == 403:
                return Response({"Message": "ACCESS-DENIED"}, status=status.HTTP_403_FORBIDDEN)

        except:
            # Case when wrong `image_id` is passed
            return Response({"Message": "Incorrect image_id"}, status=status.HTTP_404_NOT_FOUND) 


class CopyInstace(APIView):
    """
    API end-point to copy AMI image to a given region (new or the same one)
    """
    def post(self, request):
        """
        Method to copy an AMI image to a given region
        """
        image_id = self.request.data['image_id']
        print("image_id to create new instance:", image_id)

        try:
            # Create instance from given `image_id`    
            copyInstace = client.copy_image(Name='CopyAMI', Description='Test copy!!', SourceImageId=image_id, SourceRegion='us-east-2')

            if copyInstace["ResponseMetadata"]["HTTPStatusCode"] == 200:       
                return Response({"Message": "SUCCESS"}, status=status.HTTP_200_OK)
            return Response({"Message": "BAD-REQUEST"}, status=status.HTTP_400_BAD_REQUEST)

        except:
            # Case when wrong `image_id` is passed
            return Response({"Message": "Incorrect image_id"}, status=status.HTTP_404_NOT_FOUND)